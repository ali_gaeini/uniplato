import React from 'react'
import Enzyme, { mount } from 'enzyme'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { store } from '../src/Redux/store'
import Adapter from '@wojtekmaj/enzyme-adapter-react-17'
import AddButton from '../src/Components/AddButton'
Enzyme.configure({
  adapter: new Adapter(),
})
const wrapper = mount(
  <Provider store={store}>
    <AddButton />
  </Provider>
)
const findByTestAttr = (wrapper, val) => {
  return wrapper.find(`[data-test="${val}"]`)
}
it('render Card.js without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(wrapper, div)
  ReactDOM.unmountComponentAtNode(div)
})
it('shoudld has a AddCircleIcon component', () => {
  expect(findByTestAttr(wrapper, 'add-circle-icon').exists()).toEqual(true)
})
it('test add-circle-icon onClick', () => {
  const button = findByTestAttr(wrapper, 'add-circle-icon').at(0)
  button.simulate('click')
})
