import React from 'react'
import Enzyme, { mount } from 'enzyme'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { store } from '../src/Redux/store'
import Adapter from '@wojtekmaj/enzyme-adapter-react-17'
import Card from '../src/Components/Card'
Enzyme.configure({
  adapter: new Adapter(),
})
const wrapper = mount(
  <Provider store={store}>
    <Card />
  </Provider>
)
const findByTestAttr = (wrapper, val) => {
  return wrapper.find(`[data-test="${val}"]`)
}
it('render Card.js without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(wrapper, div)
  ReactDOM.unmountComponentAtNode(div)
})
it('shoudld name property contain user', () => {
  expect(findByTestAttr(wrapper, 'name').text()).toContain('user')
})
it('should has a DeleteIcon', () => {
  expect(findByTestAttr(wrapper, 'delete-icon').exists()).toEqual(true)
})
it('test delete-icon onClick', () => {
  const button = findByTestAttr(wrapper, 'delete-icon').at(0)
  button.simulate('click')
})
