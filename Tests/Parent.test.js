import React from 'react'
import Enzyme, { mount } from 'enzyme'
import ReactDOM from 'react-dom'
import Parent from '../src/Components/Parent'
import { Provider } from 'react-redux'
import { store } from '../src/Redux/store'
import Adapter from '@wojtekmaj/enzyme-adapter-react-17'

Enzyme.configure({
  adapter: new Adapter(),
})
const wrapper = mount(
  <Provider store={store}>
    <Parent />
  </Provider>
)
const findByTestAttr = (wrapper, val) => {
  return wrapper.find(`[data-test="${val}"]`)
}
it('render Parent.js without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(wrapper, div)
  ReactDOM.unmountComponentAtNode(div)
})
it('should has a Card Component', () => {
  expect(findByTestAttr(wrapper, 'card').length).toBeGreaterThanOrEqual(0)
})
it('should has a AddButton Component', () => {
  expect(findByTestAttr(wrapper, 'add-button').length).toBe(1)
})
it('shoudld has a div with Container class', () => {
  expect(findByTestAttr(wrapper, 'Container').length).toBe(2)
})
