import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import AddCircleIcon from '@material-ui/icons/AddCircle'
import { useDispatch, useSelector } from 'react-redux'
import { setCards } from '../Redux/mainActions'
const useStyles = makeStyles({
  root: {
    position: 'absolute',
    bottom: '5px',
    color: '#05b6d4',
    width: '45px',
    height: '45px',
  },
})

export default function SimpleCard() {
  const classes = useStyles()
  const reduxCards = useSelector((state) => state.cards)
  const cards = [...reduxCards]
  const dispatch = useDispatch()
  const addCard = async () => {
    cards.push({ id: cards.length + 1 })
    await dispatch(setCards(cards))
  }
  return (
    <div className={classes.root}>
      <AddCircleIcon
        data-test="add-circle-icon"
        className={classes.root}
        onClick={() => addCard()}
      />
    </div>
  )
}
