import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import DeleteIcon from '@material-ui/icons/Delete'
import { setCards } from '../Redux/mainActions'
import Draggable from 'react-draggable' // The default
const useStyles = makeStyles({
  stateOne: {
    width: '40%',
    height: '30%',
    backgroundColor: '#25292f',
    margin: '10px',
  },
  stateTwo: {
    width: '20%',
    height: '20%',
    backgroundColor: '#25292f',
    margin: '10px',
  },
  icon: {
    cursor: 'pointer',
    color: 'white',
  },
  name: {
    position: 'absolute',
    color: 'white',
    bottom: '0px',
    left: '10px',
    fontWeight: 'bold',
    fontFamily: 'sans-serif',
  },
})

export default function SimpleCard({ id }) {
  const classes = useStyles()
  const dispatch = useDispatch()
  const reduxCards = useSelector((state) => state.cards)
  const deleteItem = async (id) => {
    const cards = [...reduxCards]
    await dispatch(setCards(cards.filter((x) => x.id !== id)))
  }
  const DraggableCard = () => {
    return (
      <Draggable>
        <Card
          className={
            reduxCards.length > 6 ? classes.stateTwo : classes.stateOne
          }
          variant="outlined"
        >
          <CardContent>
            <DeleteIcon
              data-test="delete-icon"
              className={classes.icon}
              onClick={() => deleteItem(id)}
            />
          </CardContent>
          <p data-test="name" className={classes.name}>
            {' '}
            user {id}
          </p>
        </Card>
      </Draggable>
    )
  }
  return <DraggableCard />
}
