import React, { Component } from 'react'
import Card from './Card'
import AddButton from './AddButton'
import { connect } from 'react-redux'
import styled from '@emotion/styled'
const Container = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
  width: 100%;
  height: 98vh;
`
class Parent extends Component {
  render() {
    const { cards } = this.props
    return (
      <Container data-test="Container">
        {cards.map((item, index) => (
          <Card key={index} id={item.id} data-test="card" />
        ))}
        <AddButton data-test="add-button" />
      </Container>
    )
  }
}
const mapStateToProps = (state) => {
  return {
    cards: state.cards,
  }
}
export default connect(mapStateToProps, null)(Parent)
