const setCards = (value) => {
  return {
    type: 'SET-CARDS',
    value,
  }
}

export { setCards }
