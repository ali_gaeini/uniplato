const initialState = {
  cards: [],
}
const mainReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SET-CARDS':
      return { ...state, cards: action.value }
    default:
      return state
  }
}
export default mainReducer
