import { createStore, applyMiddleware } from 'redux'
import mainReducer from './mainReducer'
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2'
import thunk from 'redux-thunk'
const presistConfig = {
  key: 'root',
  storage: storage,
  stateReconciler: autoMergeLevel2,
}
const pReducer = persistReducer(presistConfig, mainReducer)
const configureStore = (initialState) => {
  return createStore(pReducer, initialState, applyMiddleware(thunk))
}
export const store = configureStore()
export const presistor = persistStore(store)
