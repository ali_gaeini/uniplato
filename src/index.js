import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import { Provider } from 'react-redux'
import { store, presistor } from './Redux/store'
import { PersistGate } from 'redux-persist/lib/integration/react'
ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <PersistGate persistor={presistor}>
        <App />
      </PersistGate>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
)
